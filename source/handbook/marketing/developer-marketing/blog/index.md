---
layout: markdown_page
title: "Developer Marketing - Blogging"
---

The [blog](/blog) is our main publishing outlet. We publish content multiple times a week. These posts include

- Short form articles
- Long form articles
- Release announcements
- Feature highlights
- Tutorials
- Inside GitLab


## The topics we write about

### Product-specific topics

- Tutorials on using GitLab, GitLab CI, etc. 
- Feature highlights bring attention to specific features at GitLab. 

### User Stories

* Customer stories 'why we choose GitLab'
* Contributor stories 'why I contribute to GitLab'
* Use case stories 'how we use GitLab'
* Boss stories 'how GitLab enabled innersourcing'
* Inception stories 'how GitLab uses GitLab'
* Adoption stories 'how we switched from SVN to GitLab'

## Community contributions

We are developing a program to invite paid contribution by GitLab community members. Coming soon!

