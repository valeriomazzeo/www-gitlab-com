---
layout: markdown_page
title: "Developer Marketing"
---

The philosophy for Developer Marketing at GitLab is to help bring insider knowledge and implicit practice to the hands of developers getting acquainted with GitLab. 

We want to meet each developer where they are now, and help them be the most efficient they can be with our tools. We want to help their teams realize their creative and collaborative goals. 

We also want to address the whole organization, whether they are technical or non-technical; at the coal-face of development or in decision making roles. GitLab is not only used for code development and review. Those same tools are used by team members to track progress, create documentation and collaborate on projects. 

We do this in the following ways:

- Maintain a [social media](/handbook/marketing/developer-marketing/social-media/) presence which is friendly and responsive.
- Publish an active [blog](/handbook/marketing/developer-marketing/blog/) with useful content relevant to GitLab users.
- Introduce [monthly themes](/handbook/marketing/developer-marketing/monthly/) exploring content from multiple perspectives, and levels of experience.
- Host [webcasts](/handbook/marketing/developer-marketing/webcasts/) which welcome newcomers, celebrate with existing users, and provide access to expertise. 
- Provide [community](/handbook/marketing/developer-marketing/community/) support for events to connect people. 
- Publish a twice-monthly newsletter.


## Our direction

You can get a peek at our to-do list for [content and engagement](content/).

2016 plans

- Webcast program
- A reliable publishing schedule on the blog
- Community meet-ups
- Get GitLab team members out to meet people in the community
- More active participation in all channels


